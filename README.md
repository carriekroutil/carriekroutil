# :wave: Welcome to Carrie Kroutil's personal GitLab space!

My name is Carrie Kroutil, and I am many things. Stick around and I'll share a bit about myself personally and professionally. :smile:

## Personal Bits

I am a wife of lucky 13 years, a mom to three awesome guys and three doggies, a daughter, a sister, a musician, a golfer, and an adveturous in a past life. Why do I say past life? Because either I have learned from those experiences or life has changed in a way that I am no loner passionate to do those activities going forward. Want some examples? Sure, keep reading the below sections...


#### 2007 - Completed the Florida Ironman

...

#### 2006 - Attempted to do an MS150

...

#### 2002 - Rock climbed to the top of Devils Tower

...

#### 2001 - Hiked my first 14er, Longs Peak in Colorado

...

## Professional Bits

I am greatful to currently work as a Fullstack Engineering Manager at GitLab! 

I started at GitLab in 2022 and you can follow my :footprints: journey here: @ckroutil. I have some intersting transparent projects which shares how I work and things learned along the way. Check them out!

- [My Planner](https://gitlab.com/ckroutil/planner) - where I create a [weekly issue](https://gitlab.com/ckroutil/planner/-/issues/7) to track my tasks and accomplishments. 
- [Dogs](https://gitlab.com/ckroutil/dogs) - my first static site on GitLab using GitLab Pages.
- [Coffee Chats](https://gitlab.com/ckroutil/ckroutil/-/blob/main/coffee-chats.md) - A list of people I have gotten to know across the company so far.

My team is the Package Registry group that is part of the [Package stage](https://about.gitlab.com/handbook/engineering/development/ops/package/).

More to come!


